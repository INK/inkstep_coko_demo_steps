#spec_helper.rb

def temp_directory
  @temp_directory ||= "/tmp/test/#{Time.now.to_i}"
end

def create_directory_if_needed(path)
  FileUtils.mkdir_p(path) unless File.directory?(path)
end