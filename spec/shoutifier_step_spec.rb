require 'spec_helper'
require 'coko_demo_steps/ink_step/coko/shoutifier_step'

describe InkStep::Coko::ShoutifierStep do
  let(:target_file_name)      { "party.txt" }

  let(:target_file)           { File.join(Dir.pwd, "spec", "fixtures", "files", target_file_name) }
  let!(:input_directory)    { File.join(temp_directory, InkStep::Base::INPUT_FILE_DIRECTORY_NAME) }

  before do
    create_directory_if_needed(input_directory)
    FileUtils.cp(target_file, input_directory)
  end

  subject               { InkStep::Coko::ShoutifierStep.new(chain_file_location: temp_directory, position: 1) }

  after :each do
    FileUtils.rm_rf(FileUtils.rm_rf(Dir.glob("#{temp_directory}/*")))
  end

  describe '#perform_step' do

    context 'with no parameters' do
      context 'when converting a HTML file' do

        let(:target_file_name)      { "some_text.html" }

        it 'returns a rot13 version of the text' do
          subject.perform_step

          result = File.new(File.join(subject.send(:working_directory), "some_text.html"), 'r')
          expect(File.read(result)).to eq shouty_html
        end
      end

      context 'when converting a text file' do

        let(:target_file_name)      { "party.txt" }

        it 'performs rot13 on the whole file' do
          subject.perform_step

          result = File.new(File.join(subject.send(:working_directory), "party.txt"), 'r')
          expect(File.read(result)).to eq shouty_text
        end
      end
    end

    context 'with parameters' do

      context 'when converting a text file' do

        let(:target_file_name)      { "party.txt" }

        it 'performs rot13 on the whole file' do
          subject.combined_parameters = {punctuation: "...!!1"}
          subject.perform_step

          result = File.new(File.join(subject.send(:working_directory), "party.txt"), 'r')
          expect(File.read(result)).to eq shouty_text_with_param
        end
      end
    end
  end

  describe '#version' do
    specify do
      expect{subject.version}.to_not raise_error
    end
  end

  describe '#description' do
    specify do
      expect{subject.class.description}.to_not raise_error
    end
  end

  describe '#human_readable_name' do
    specify do
      expect{subject.class.human_readable_name}.to_not raise_error
    end
  end

  def shouty_text
<<TEXT
THIS EXAMPLE FILE IS EXCITING!!! YES IT IS!!!
LET'S HAVE A PARTY!!! I'LL BRING SOME ICE CREAM!!!
SEE YOU TOMORROW!!!
TEXT
  end

  def shouty_text_with_param
<<TEXT
THIS EXAMPLE FILE IS EXCITING...!!1 YES IT IS...!!1
LET'S HAVE A PARTY...!!1 I'LL BRING SOME ICE CREAM...!!1
SEE YOU TOMORROW...!!1
TEXT
  end

  def shouty_html
<<TEXT
<html>
<head><title>A TITLE</title></head>
<body><h1>HERE IS AN H1</h1>
<p>HERE IS A PARAGRAPH!!! LOOK AT IT GO!!!</p></body>
</html>
TEXT
  end
end