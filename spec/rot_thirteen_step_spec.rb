require 'spec_helper'
require 'coko_demo_steps/ink_step/coko/rot_thirteen_step'

describe InkStep::Coko::RotThirteenStep do
  let(:target_file_name)      { "some_text.txt" }
  subject               { InkStep::Coko::RotThirteenStep.new(chain_file_location: temp_directory, position: 1) }

  let(:target_file)           { File.join(Dir.pwd, "spec", "fixtures", "files", target_file_name) }
  let!(:input_directory)    { File.join(temp_directory, InkStep::Base::INPUT_FILE_DIRECTORY_NAME) }

  before do
    create_directory_if_needed(input_directory)
    FileUtils.cp(target_file, input_directory)
  end

  after :each do
    FileUtils.rm_rf(FileUtils.rm_rf(Dir.glob("#{temp_directory}/*")))
  end

  describe '#perform_step' do
    before do
      create_directory_if_needed(input_directory)
      FileUtils.cp(target_file, input_directory)
    end

    context 'when converting a HTML file' do

      let(:target_file_name)      { "some_text.html" }

      it 'returns a rot13 version of the text' do
        subject.perform_step

        result = File.new(File.join(subject.send(:working_directory), "some_text_rot13.html"), 'r')
        expect(File.read(result)).to eq rot13_html
      end
    end

    context 'when converting a text file' do

      let(:target_file_name)      { "some_text.txt" }

      it 'performs rot13 on the whole file' do
        subject.perform_step

        result = File.new(File.join(subject.send(:working_directory), "some_text_rot13.txt"), 'r')
        expect(File.read(result)).to eq rot13_text
      end
    end
  end

  describe '#version' do
    specify do
      expect{subject.version}.to_not raise_error
    end
  end

  describe '#description' do
    specify do
      expect{subject.class.description}.to_not raise_error
    end
  end

  describe '#human_readable_name' do
    specify do
      expect{subject.class.human_readable_name}.to_not raise_error
    end
  end

  def rot13_text
<<TEXT
Guvf svyr unf fbzr grkg
Bayl tbbq sbe n qrzb
Vg vf obevat nf
TEXT
  end

  def rot13_html
<<TEXT
<html>
<head><title>N gvgyr</title></head>
<body><h1>Urer vf na U1</h1>
<p>Urer vf n cnentencu. Ybbx ng vg tb.</p></body>
</html>
TEXT
  end
end