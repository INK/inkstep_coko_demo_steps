# InkStep - CokoDemoSteps.
### Collaborative Knowledge Foundation

This is a step gem that is meant to run as a plugin for [the Ink API](https://gitlab.coko.foundation/INK/ink-api). If you are looking to write your own step to plug into your own instance of INK, this is a good place to start. 

## Steps included:

### RotThirteenStep
This is a step for running a very simple obfuscation algorithm, like on Usenet back in the day if you wanted to post spoilers :)

### ShoutifierStep
Makes everything shouty. => MAKES EVERYTHING SHOUTY!!! 
When you really want to get your point across and don't feel like getting out a thesaurus.

## Installation
 
See [the Ink documentation](https://gitlab.coko.foundation/INK/ink-api) for detailed instructions for installing this plugin into an instance of INK.  

## Development

If you are wanting to use this step as a template to make your own, instructions to do so have been left in the individual files.

The main components of this step are:

# `lib/coko_demo_steps/inkstep_coko_demo_steps.rb`

This file allows Rails to run this gem as an engine (if it exists), or running it as part of an alternative Ruby framework (e.g. Sinatra) by autoloading all the files. So you don't have to `require` everything.

# `lib/coko_demo_steps/engine.rb`

This file is what Rails would run to autoload the `*.lib` files automatically.

# `lib/coko_demo_steps/ink_step/rot_thirteen_step.rb`

INK convention is to put all step files into the `InkStep` module (and matching `ink_step` directory).

This file is where the step logic for `RotThirteenStep` actually lives. For a conversion step, subclass `InkStep::ConversionStep`. For a validation step, subclass `InkStep::ValidationStep`.

Put the logic into the `perform_step` method.

# `lib/coko_demo_steps/version.rb`

This is the version of the gem. Please increment whenever you update, even if the change is minor or if you don't publish on RubyGems. Versioning allows ink users to debug possible issues if they know what version of the step they are running, or have run in the past. 

That's all! Enjoy. 

Charlie Ablett
Collaborative Knowledge Foundation
charlie@coko.foundation