# this allows Rails (if this is being used in rails!)
# to just automatically start using the step classes
# (or anything in lib) without having to require it.
# Makes it far more extensible!

module CokoDemoSteps
  class Engine < ::Rails::Engine
    config.eager_load_paths += Dir["#{config.root}/lib/**/"]
  end
end