require 'coko_demo_steps/version'
require 'ink_step/conversion_step'

module InkStep::Coko
  class ShoutifierStep < InkStep::ConversionStep

    def perform_step
      super
      # Find the target file
      source_file_path = find_source_file(regex: [/\.text$/, /\.html/, /\.txt$/, /\.htm$/])
      original_file = File.join(working_directory, source_file_path)
      output_file_path = original_file # This will overwrite the given file.

      # Grab the contents of the file
      contents = read_file(original_file)
      # Clear the contents (we'll write back to it)
      File.truncate(original_file, 0)
      # Apply rot13 (see core_extensions/string.rb)
      punctuation_param = parameter(:punctuation)
      result = contents.shoutify(punctuation: punctuation_param)
      log_as_step "Applying ROT-13 to #{source_file_path}..."
      # Write the file to the working directory.
      # the folder working_directory is for everything your step does. It's the sandbox for your step.
      # Each step has a unique directory that working_directory points to, so you don't have to worry about different steps overwriting each others' files.
      # your step can overwrite its own files, that is fine.
      file = File.new(output_file_path, 'w+')
      File.write(file, result)
      success!
    end

    def version
      CokoDemoSteps::VERSION
    end

    def self.description
      "CAPITALISES ALL TEXT in document (text, txt, html) and replaces periods with punctuation of your choice."
    end

    def self.human_readable_name
      "SHOUTIFIER!!!1"
    end

    def accepted_parameters
      # e.g. {foo: "For setting the grobblegronx measure", bar: "Can be X, Y or Z"}
      {punctuation: "The value a period will be replaced with."}
    end

    def default_parameter_values
      # e.g. {foo: 1, bar: nil}
      {punctuation: "!!!"}
    end

    def required_parameters
      [] # none!
    end

    protected

    def read_file(input_file)
      if input_file.respond_to? :read
        input_file.read
      elsif input_file.is_a?(String) && File.exist?(input_file)
        File.read(input_file)
      else
        raise "cannot read '#{input_file}'"
      end
    end
  end
end

class String
  def shoutify(punctuation:, format_html: true)
    inside_html_tag = false
    raise "The file is either empty or does not have text" if length == 0
    split('').inject('') do |text, char|
      text << case char
                when 'a'..'z', 'A'..'Z'
                  if inside_html_tag && format_html
                    char
                  else
                    char.upcase
                  end
                when '.'
                  punctuation
                when '>'
                  inside_html_tag = false
                  char.ord
                when '<'
                  inside_html_tag = true
                  char.ord
                else
                  char.ord
              end
    end
  end
end