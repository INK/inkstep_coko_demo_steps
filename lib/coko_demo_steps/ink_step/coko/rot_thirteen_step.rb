require 'coko_demo_steps/version'
require 'ink_step/conversion_step'
require 'coko_demo_steps/core_extensions/string'

module InkStep::Coko
  class RotThirteenStep < InkStep::ConversionStep

    def perform_step
      super
      # Find the target file
      source_file_path = find_source_file(regex: [/\.text$/, /\.html/, /\.txt$/, /\.htm$/])
      source_file_extension = Pathname(source_file_path).extname
      source_file_name = Pathname(source_file_path).sub_ext ''
      output_file_path = File.join(working_directory, "#{source_file_name}_rot13#{source_file_extension}")

      # Grab the contents of the file
      contents = read_file(File.join(working_directory, source_file_path))
      # Apply rot13 (see core_extensions/string.rb)
      result = contents.rot13
      log_as_step "Applying ROT-13 from #{source_file_path} to #{source_file_name}_rot13#{source_file_extension}"
      # Write the file to the working directory.
      # the folder working_directory is for everything your step does. It's the sandbox for your step.
      # Each step has a unique directory that working_directory points to, so you don't have to worry about different steps overwriting each others' files.
      # your step can overwrite its own files, that is fine.
      file = File.new(output_file_path, 'w+')
      File.write(file, result)
      # This step cleans up after itself by destroying the original file. The INK framework had copied the original file from
      # either the previous step or the chain's input file directory. So it's OK for the step to clean up after itself in this way,
      # and only pass on files that are relevant.
      cleanup!(File.join(working_directory, source_file_path))
      success!
    end

    def accepted_parameters
      # e.g. {foo: "For setting the grobblegronx measure", bar: "Can be X, Y or Z"}
      { } # none for this step. I don't even have to include this method.
    end

    def default_parameter_values
      # e.g. {foo: 1, bar: nil}
      { } # none for this step. I don't even have to include this method.
    end

    def required_parameters
      [] # none!
    end

    def version
      CokoDemoSteps::VERSION
    end

    # this method must be present and return a string - a brief description of what the step does.

    def self.description
      "Applies 'rot-13' algorithm to text in the document (html, text, txt)"
    end

    def self.human_readable_name
      "ROT-13 Cipher"
    end

    protected

    def cleanup!(file)
      File.delete(file)
    end

    def read_file(input_file)
      if input_file.respond_to? :read
        input_file.read
      elsif input_file.is_a?(String) && File.exist?(input_file)
        File.read(input_file)
      else
        raise "cannot read '#{input_file}'"
      end
    end
  end
end