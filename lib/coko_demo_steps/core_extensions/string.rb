# from https://gist.github.com/rwoeber/274126

class String
  def rot13(format_html = true)
    inside_html_tag = false
    split('').inject('') do |text, char|
      text << case char
                when 'a'..'m', 'A'..'M'
                  if inside_html_tag && format_html
                    char.ord
                  else
                    char.ord + 13
                  end
                when 'n'..'z', 'N'..'Z'
                  if inside_html_tag && format_html
                    char.ord
                  else
                    char.ord - 13
                  end
                when '>'
                  inside_html_tag = false
                  char.ord
                when '<'
                  inside_html_tag = true
                  char.ord
                else
                  char.ord
              end
    end
  end
end