# Modify the module name if you are using this as a template to write your own steps.

module CokoDemoSteps
  if defined?(Rails)
    require 'coko_demo_steps/engine'
  else
    # require any files that need to be exposed to any non-Rails environments here (e.g. test, Sinatra, etc)
    require 'coko_demo_steps/ink_step/coko/rot_thirteen_step'
    require 'coko_demo_steps/ink_step/coko/shoutifier_step'

    #... anything else
  end
end