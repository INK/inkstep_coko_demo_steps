# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'coko_demo_steps/version'

Gem::Specification.new do |spec|
  spec.name           = 'inkstep_coko_demo_steps'
  spec.version        = CokoDemoSteps::VERSION
  spec.date           = '2016-10-28'
  spec.summary        = "A couple of basic steps for demonstration purposes"
  spec.description    = "This gem includes steps for applying ROT-13 to text, like on Usenet back in the day if you wanted to post spoilers. It also contains a SHOUTIFIER step for MAKING EVERYTHING SHOUTY!!!"
  spec.authors        = ["Charlie Ablett"]
  spec.email          = 'charlie@coko.foundation'
  spec.homepage       = 'https://gitlab.coko.foundation/INK/inkstep_coko_demo_steps'
  spec.license        = 'MIT'

  spec.executables    = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files     = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths  = %w(lib)

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "rake"

  spec.required_ruby_version = '~> 2.2'
  spec.files         = Dir.glob("{lib}/**/*") + %w(./README.md)
end
